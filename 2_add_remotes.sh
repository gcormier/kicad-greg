cd kicad-footprints
git remote add upstream https://gitlab.com/kicad/libraries/kicad-footprints
cd ..

cd kicad-packages3D
git remote add upstream https://gitlab.com/kicad/libraries/kicad-packages3D
cd ..

cd kicad-symbols
git remote add upstream https://gitlab.com/kicad/libraries/kicad-symbols
cd ..

cd kicad-espressif
git remote add upstream https://github.com/espressif/kicad-libraries
cd ..
