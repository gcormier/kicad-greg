git fetch --recurse-submodules

echo Footprints...
cd kicad-footprints
git fetch upstream
git checkout master
git merge upstream/master
git push
cd ..

echo
echo Symbols...

cd kicad-symbols
git fetch upstream
git checkout master
git merge upstream/master
git push
cd ..

echo
echo 3D...
cd kicad-packages3D
git fetch upstream
git checkout master
git merge upstream/master
git push
cd ..

echo
echo espressif...
cd kicad-espressif
git fetch upstream
git checkout main
git merge upstream/main
git push
cd ..




echo
echo
echo Done!
sleep 4